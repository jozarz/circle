class CircleGenerator
  def initialize(number)
    @number = Integer(number)
  end

  def call
    CircleModel.new(color)
  end

  private

  def color
    case
    when @number % 15 == 0 then :purple
    when @number % 5 == 0 then :blue
    when @number % 3 == 0 then :green
    else :pink
    end
  end
end
