class ApplicationEndpoint
  def call(env)
    @env = env
    fetch_params
    validation_result = validate_params
    return validation_result if @error
    process
  end

  private

  def fetch_params
    @params = Rack::Request.new(@env).params
  end

  def error(status, message)
    @error = true
    response(status, { message: message }.to_json)
  end

  def present(object)
    response(200, object.to_json)
  end

  def response(status, body)
    [status, { 'Content-Type' => 'text/json' }, [body]]
  end
end
