class CircleModel
  POINTS_MAPPING = {
    pink: 1,
    green: 3,
    blue: 5,
    purple: 15
  }

  attr_reader :color

  def initialize(color)
    @color = color.to_sym
  end

  def points
    POINTS_MAPPING[@color]
  end

  def to_json
    { color: color, points: points }.to_json
  end
end
