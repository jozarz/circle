class CircleEndpoint < ApplicationEndpoint
  private

  def process
    circle = CircleGenerator.new(@params['count']).call
    present circle
  end

  def validate_params
    return error(400, 'Please provide count param') if @params['count'].nil?
    return error(400, 'Count param should be an integer') unless integer?(@params['count'])
    return error(400, 'Count param should be positive integer') if Integer(@params['count']) < 1
  end

  def integer?(value)
    Integer(value)
    true
  rescue ArgumentError
    false
  end
end
