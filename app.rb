require 'json'
require 'rack'
require 'rack/static'

require_relative 'app/circle_model'
require_relative 'app/circle_generator'
require_relative 'app/application_endpoint'
require_relative 'app/circle_endpoint'

CircleApplication = Rack::Builder.new do
  use Rack::Static, urls: ['/assets'], root: 'public', index: 'main.html'
  map '/api/circle' do
    run CircleEndpoint.new
  end
end
