require_relative 'spec_helper'

describe '/api/circle' do
  shared_examples 'bad request' do
    it 'has status 400' do
      expect(subject.status).to eq 400
    end
  end

  let(:params) { {} }

  subject(:request) { server.get('/api/circle', params: params) }

  it 'has json content' do
    expect(subject.headers['Content-Type']).to eq 'text/json'
  end

  context 'when count param is missing' do
    it_behaves_like 'bad request'

    it 'has correct message' do
      expect(JSON.parse(subject.body)).to eq('message' => 'Please provide count param')
    end
  end

  context 'when count param is present' do
    context 'when it is not a number' do
      let(:params) { { count: 'not-number' } }

      it_behaves_like 'bad request'

      it 'has correct message' do
        expect(JSON.parse(subject.body)).to eq('message' => 'Count param should be an integer')
      end
    end

    context 'when it is not positive' do
      let(:params) { { count: 0 } }

      it_behaves_like 'bad request'

      it 'has correct message' do
        expect(JSON.parse(subject.body)).to eq('message' => 'Count param should be positive integer')
      end
    end

    context 'when it is a positive integer' do
      let(:params) { { count: 1 } }

      it 'has status 200' do
        expect(subject.status).to eq 200
      end

      describe 'body' do
        subject(:body) { JSON.parse(request.body) }

        it 'should have color and points fields' do
          expect(body.keys).to match_array %w(points color)
        end

        [
          [1, 'pink', 1], [2, 'pink', 1], [3, 'green', 3],
          [4, 'pink', 1], [5, 'blue', 5], [7, 'pink', 1],
          [15, 'purple', 15], [50, 'blue', 5], [63, 'green', 3],
          [90, 'purple', 15], [111, 'green', 3], [113, 'pink', 1]
        ].each do |count, color, points|
          context "when it is equal to #{count}" do
            let(:params) { { count: count } }

            it "has #{color} color" do
              expect(body['color']).to eq color
            end

            it "has #{points} points" do
              expect(body['points']).to eq points
            end
          end
        end
      end
    end
  end
end
