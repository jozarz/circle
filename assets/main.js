import App from './App'
import './App.scss'

document.addEventListener('DOMContentLoaded', () => {
  const app = new App(document.getElementById('app'));
  app.render();
});