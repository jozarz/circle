export function get(path, count) {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    path = `${path}?count=${count}`;
    request.open('GET', path);

    request.onload = () => {
      const data = JSON.parse(request.response);
      const response = { data,  status: request.status }
      request.status < 300 ? resolve(response) : reject(response);
    };
    request.send();
  })
}