import { get } from './request'

import './App.scss'

export default class App {
  constructor(mountElement) {
    this._mountElement = mountElement;
    this.state = { score: 0, circles: [] }
  }

  render() {
    this._mountElement.innerHTML = this.html();
    this.initButton();
    this.initScore();
    this.initSum();
    this.initCirclesContainer()
  }

  rerender() {
    this.renderSum();
    this.renderScore();
    this.updateContainer();
  }

  drop() {
    const { circles } = this.state;
    get('/api/circle', circles.length + 1)
    .then((response) => {
      const { color, points } = response.data;
      this.state.circles.push({ color });
      this.state.score += points;
      this.rerender();
    });
  }
  initButton() {
    this._button = this._mountElement.querySelector('#drop-button')
    this._button.addEventListener('click', () => this.drop())
  }

  initSum() {
    this._sum = this._mountElement.querySelector('#sum')
    this.renderSum();
  }

  initScore() {
    this._score = this._mountElement.querySelector('#score')
    this.renderScore();
  }

  initCirclesContainer() {
    this._circlesContainer = this._mountElement.querySelector('#circles-container')
  }

  renderSum() {
    const { circles } = this.state;
    this._sum.innerHTML = `Sum ${circles.length}`
  }

  renderScore() {
    const { score } = this.state;
    this._score.innerHTML = `Sum ${score}`
  }

  updateContainer() {
    const { circles } = this.state;
    const circle = circles[circles.length - 1]
    const circleWrapper = document.createElement('div');
    circleWrapper.className = 'circle__wrapper';
    circleWrapper.innerHTML = `<div class="circle circle--${circle.color}"></div>`;
    this._circlesContainer.prepend(circleWrapper);
  }

  html() {
    return(
        `<div class="app">
          <div class="app__wrapper">
            <div class="header">
              <div id='sum' class="header__element"></div>
              <div class="header__element">
                <button id="drop-button">Drop</button>
              </div>
              <div id='score' class="header__element"/></div> 
            </div>
            <div id="circles-container" class="circles-container"></div>
          </div>
        </div>`
    )
  }
}