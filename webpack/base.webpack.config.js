var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var rootPath = path.join(__dirname, '..');

module.exports = {
  entry: {
    'main': './assets/main.js',
  },
  output: {
    path: path.join(rootPath, 'public', 'assets'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['*', '.js'],
  },

  plugins: [
    new ExtractTextPlugin({filename: 'main.css'})
  ],
  module: {
    loaders: [
      {
        test: /.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.scss$/,
        loaders: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!sass-loader'})
      }
    ]
  },
}
